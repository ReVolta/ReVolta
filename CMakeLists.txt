cmake_minimum_required( VERSION 3.23 )

option( LIBREVOLTA_POSTBUILD_INSTALL "Automatically install librevolta once built" ON )
option( LIBREVOLTA_ADD_DEPENDENCY "Dependency to librevolta build target" ON )

# Those are the configuration defaults. Once the values are specified
# using -D... directive, it is being set in cache. The statements below just take effect
# once there is no entry of such variable in the cache (given by -D directive).
# So effectively, the values below are used only if not set "from outside"
set( CMAKE_TARGET_PROCESSOR "x86" CACHE STRING "Target processor" )
message( STATUS "Target processor is ${CMAKE_TARGET_PROCESSOR}" )

# C++20 Support
set( CMAKE_CXX_STANDARD 20 )
set( CMAKE_CXX_STANDARD_REQUIRED ON )
set( CMAKE_CXX_EXTENSIONS OFF )
# Report used C++ standard
message( STATUS "Using C++${CMAKE_CXX_STANDARD}" )

# Configuring the builds for the target, not testing environment 
if( NOT ${CMAKE_BUILD_TYPE} STREQUAL "Test" )
	# Set crosscompiling toolchain - the triplet to be used to generate appropriate toolchain file
	set( REVOLTA_TOOLCHAIN_TRIPLET "i686-revolta" CACHE STRING "Toolchain triplet" )
	message( STATUS "Using ${REVOLTA_TOOLCHAIN_TRIPLET} toolchain" )

	# Set CMake modules location
	set( CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/tools/cmake/modules" )
	set( REVOLTA_TOOLCHAIN_ROOT "${CMAKE_SOURCE_DIR}/toolchain/${REVOLTA_TOOLCHAIN_TRIPLET}" )
	set( CMAKE_TOOLCHAIN_FILE "${CMAKE_MODULE_PATH}/Toolchain/${REVOLTA_TOOLCHAIN_TRIPLET}.cmake" )

	# Generate toolchain file 
	configure_file( "${CMAKE_MODULE_PATH}/Toolchain/toolchain.cmake.in" ${CMAKE_TOOLCHAIN_FILE} ) 
	set_source_files_properties( ${CMAKE_TOOLCHAIN_FILE} PROPERTIES GENERATED TRUE )

	# Set SYSROOT directory
    # TODO
	message( STATUS "ReVolta sysroot directory is ${CMAKE_SYSROOT}" )
endif()

set( REVOLTA_ROOT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/source" )

project( ReVolta )

# Enable testing and set BUILD_TESTING variable (by CTest inclusion)
if( ${CMAKE_BUILD_TYPE} STREQUAL "Test" )
	include( CTest )
	# Find Google Test package
	find_package( GTest REQUIRED )
else()
	set( BUILD_TESTING OFF )
endif()

# librevolta
add_subdirectory( "${REVOLTA_ROOT_SOURCE_DIR}/librevolta" "${CMAKE_CURRENT_BINARY_DIR}/librevolta" )
# ReVolta kernel
add_subdirectory( "${REVOLTA_ROOT_SOURCE_DIR}/kernel" "${CMAKE_CURRENT_BINARY_DIR}/kernel" )

if( NOT BUILD_TESTING )
	# Read out kernel binary filename global property set within the kernel build itself
	get_property( KERNEL_FILENAME GLOBAL PROPERTY REVOLTA_KERNEL_BINARY_FILENAME )
	get_property( BOOTSTRAP_FILENAME GLOBAL PROPERTY REVOLTA_BOOTSTRAP_BINARY_FILENAME )
#	get_property( REVOLTA_APPS_TARGET GLOBAL PROPERTY REVOLTA_INITR_APPS_TARGET )
	# Generate Grub configuration file
	if( ${REVOLTA_USE_MULTIBOOT2} )
		set( REVOLTA_MULTIBOOT_GRUB_COMMAND "multiboot2" )
	else()
		set( REVOLTA_MULTIBOOT_GRUB_COMMAND "multiboot" )
	endif()
	configure_file( ${CMAKE_SOURCE_DIR}/tools/grub/grub.cfg.in ${CMAKE_SOURCE_DIR}/iso/boot/grub/grub.cfg )
	# Create ISO file to be run
	add_custom_target( iso
#		ALL
		COMMAND mkdir -p ${CMAKE_SOURCE_DIR}/iso/boot/grub
		COMMAND grub-mkrescue -d ${CMAKE_SOURCE_DIR}/tools/grub/modules/${CMAKE_TARGET_PROCESSOR}-pc -o ${CMAKE_SOURCE_DIR}/revolta.iso ${CMAKE_SOURCE_DIR}/iso
		DEPENDS 
			${BOOTSTRAP_FILENAME} 						# Bootstrap binary filename
			${KERNEL_FILENAME} 							# Kernel binary filename
			${CMAKE_SOURCE_DIR}/iso/boot/grub/grub.cfg	# Grub configuration file
		COMMENT "Creating ReVolta ISO image"
		VERBATIM
	)
else()
	# Add test source directories
	add_subdirectory( "${CMAKE_CURRENT_SOURCE_DIR}/test" )
endif()
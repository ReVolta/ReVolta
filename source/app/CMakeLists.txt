
# GUIDELINE:
# In order to pack the application to the initrd:
# 1) The application must append itself to the REVOLTA_APPS_EXECUTABLES
#    list including the path where in the initrd image it has to be placed.
# 2) The application's executable must be outputted to "${CMAKE_BINARY_DIR}/initrd/"

# Non testable code
if( NOT BUILD_TESTING )
	add_subdirectory( init )
endif()

# All userspace initrd applications target name
set( REVOLTA_APPS_TARGET "apps" )
# Setup global property to publish REVOLTA_APPS_TARGET in global scope
set_property( GLOBAL PROPERTY REVOLTA_INITR_APPS_TARGET ${REVOLTA_APPS_TARGET} )

# Define custom target to pack all executables into initrd tar archive
add_custom_target( ${REVOLTA_APPS_TARGET}
	COMMAND tar --create --verbose --file=${CMAKE_SOURCE_DIR}/iso/boot/initrd.tar --directory=${CMAKE_BINARY_DIR}/initrd/ ${REVOLTA_APPS_EXECUTABLES} 
	DEPENDS ${REVOLTA_APPS_EXECUTABLES}
	COMMENT "Packing all the applications into TAR archive"
	VERBATIM
)
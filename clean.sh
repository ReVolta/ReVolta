#! /bin/sh

confirmation_enabled=true

do_clean_sysroot=false
do_clean_build=false
do_clean_tools=false

for argument in "$@"
do
case $argument in
    --sysroot*) do_clean_sysroot=true shift ;;
    --with-sysroot-directory=*) sysroot_directory="${argument#*=}" shift ;;
    --build*) do_clean_build=true shift ;;
    --with-build-directory=*) build_directory="${argument#*=}" shift ;;
    --tools*) do_clean_tools=true shift ;;
    --with-tools-directory=*) tools_directory="${argument#*=}" shift ;;
    --disable-confirmation*) confirmation_enabled=false shift ;;
    *) echo "Unknown option detected. See --help for correct setup.\n"; exit ;;
esac
done

# Check for sysroot value and set default once not specified
if [ ! ${sysroot_directory} ]; then sysroot_directory=${PWD}/sysroot; fi
if [ ! ${build_directory} ]; then build_directory=${PWD}/build; fi
if [ ! ${tools_directory} ]; then tools_directory=${PWD}/staging; fi

clean_sysroot () {
	if [ ${confirmation_enabled} = true ]; then
		read -p "Are you sure to delete all the content in \"${sysroot_directory}\"? (y/N): " answer
	else
		answer="y"
	fi
	
	# No is the default answer to be on a safe side
	[ -z "$answer" ] && answer="n"

	case ${answer} in
		[Yy]* )
			echo "Cleaning up SYSROOT in ${sysroot_directory}"
			find ${sysroot_directory}/boot/* -delete #-print 
			find ${sysroot_directory}/usr/include/* -delete
			find ${sysroot_directory}/usr/lib/* -delete
			echo "...done."
			break;;
		[Nn]* )
			echo "Nothing cleaned."
			break;;
	esac
}

clean_build () {
	if [ ${confirmation_enabled} = true ]; then
		read -p "Are you sure to delete all the content in \"${build_directory}\"? (y/N): " answer
	else
		answer="y"
	fi
	
	# No is the default answer to be on a safe side
	[ -z "$answer" ] && answer="n"

	case ${answer} in
		[Yy]* )
			echo "Cleaning build directory in ${build_directory}"
			find ${build_directory}/* -delete #-print
			echo "...done."
			break;;
		[Nn]* )
			echo "Nothing cleaned."
			break;;
	esac
}

clean_tools () {
	if [ ${confirmation_enabled} = true ]; then
		read -p "Are you sure to delete all the content in \"${tools_directory}\"? (y/N): " answer
	else
		answer="y"
	fi
	
	# No is the default answer to be on a safe side
	[ -z "$answer" ] && answer="n"

	case ${answer} in
		[Yy]* )
			echo "Cleaning tools directory in ${tools_directory}"
			echo "Cleaning of tools is not yet supported."
			break;;
		[Nn]* )
			echo "Nothing cleaned."
			break;;
	esac
}

if [ ${do_clean_sysroot} = true ]; then clean_sysroot; fi
if [ ${do_clean_build} = true ]; then clean_build; fi
if [ ${do_clean_tools} = true ]; then clean_tools; fi
